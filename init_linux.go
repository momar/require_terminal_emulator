package require_terminal_emulator

import (
	"os"
	"os/exec"
	"strings"
)

// +build linux

func init() {
	if os.Getenv("TERM") == "" {
		args := append([]string{}, os.Args...)
		for i := range args {
			args[i] = "'" + strings.ReplaceAll(args[i], "'", "'\"'\"'") + "'"
		}
		if exec.Command("x-terminal-emulator", append([]string{"-e"}, args...)...).Start() == nil {
			os.Exit(125)
		}
	}
}
